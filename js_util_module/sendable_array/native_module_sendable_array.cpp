 /*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "js_native_api.h"
#include "utils/log.h"
#include "napi/native_api.h"
#include "napi/native_node_api.h"

// extern const char _binary_json_js_js_start[];
// extern const char _binary_json_js_js_end[];
// extern const char _binary_json_abc_start[];
// extern const char _binary_json_abc_end[];

static napi_value ArkTSCollectionsInit(napi_env env, napi_value exports)
{
    napi_value global;
    napi_value sharedArrayKey;
    napi_value sharedArrayValue;
    napi_get_global(env, &global);
    const std::string sharedArrayName =  "SharedArray";
    napi_create_string_utf8(env, sharedArrayName.c_str(), sharedArrayName.size(), &sharedArrayKey);
    napi_get_property(env, global, sharedArrayKey, &sharedArrayValue);

    napi_value sharedSetKey;
    napi_value sharedSetValue;
    const std::string sharedSetName =  "SharedSet";
    napi_create_string_utf8(env, sharedSetName.c_str(), sharedSetName.size(), &sharedSetKey);
    napi_get_property(env, global, sharedSetKey, &sharedSetValue);

    napi_value sharedMapKey;
    napi_value sharedMapValue;
    const std::string sharedMapName =  "SharedMap";
    napi_create_string_utf8(env, sharedMapName.c_str(), sharedMapName.size(), &sharedMapKey);
    napi_get_property(env, global, sharedMapKey, &sharedMapValue);

    // 
    napi_property_descriptor desc[] = {
        DECLARE_NAPI_PROPERTY("Array", sharedArrayValue),
        DECLARE_NAPI_PROPERTY("Map", sharedMapValue),
        DECLARE_NAPI_PROPERTY("Set", sharedSetValue),
    };
    napi_define_properties(env, exports, sizeof(desc) / sizeof(desc[0]), desc);
    return exports;
}

// extern "C"
// __attribute__((visibility("default"))) void NAPI_util_json_GetJSCode(const char **buf, int *bufLen)
// {
//     if (buf != nullptr) {
//         *buf = _binary_json_js_js_start;
//     }

//     if (bufLen != nullptr) {
//         *bufLen = _binary_json_js_js_end - _binary_json_js_js_start;
//     }
// }
// extern "C"
// __attribute__((visibility("default"))) void NAPI_util_json_GetABCCode(const char** buf, int* buflen)
// {
//     if (buf != nullptr) {
//         *buf = _binary_json_abc_start;
//     }
//     if (buflen != nullptr) {
//         *buflen = _binary_json_abc_end - _binary_json_abc_start;
//     }
// }

static napi_module_with_js sendableArrayModule = {
    .nm_version = 1,
    .nm_flags = 0,
    .nm_filename = nullptr,
    .nm_register_func = ArkTSCollectionsInit,
    .nm_modname = "arkts.collections", // @ohos.arkts.collections Array
    .nm_priv = ((void*)0),
    // .nm_get_abc_code = NAPI_util_json_GetABCCode,
    // .nm_get_js_code = NAPI_util_json_GetJSCode,
};

extern "C" __attribute__ ((constructor)) void JsonRegisterModule()
{
    napi_module_with_js_register(&sendableArrayModule);
}
